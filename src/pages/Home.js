import Banner from '../Components/Banner';
import Highlights from '../Components/Highlights';
import CourseCard from '../Components/CourseCard';
import {Fragment} from 'react';

export default function Home(){
	return (
		<Fragment>
			<Banner />
			<Highlights />
			<CourseCard />
		</Fragment>	
		)
}
