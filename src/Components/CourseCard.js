import {Card, Button} from 'react-bootstrap';

export default function CourseCard(){
	return(
		<Card>
		  <Card.Body>
		    <Card.Title>Vue.js Short Course</Card.Title>
		    <Card.Text>
		      <p class="heavyFont">Description:</p>
		      <p>This 2-month course will teach future developers about Vue.js from beginner to advanced level</p>
		      <p class="heavyFont">Price:</p>
		      Php 10000
		    </Card.Text>
		    <Button variant="primary" href="#enroll">Enrol</Button>
		  </Card.Body>
		</Card>
		)
}